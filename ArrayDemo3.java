package Array;

public class ArrayDemo3 {
	public static void main(String[] args) {
		int marks[] = new int[5];

		ArrayFunction.PrintArrays(marks);
		ArrayFunction.setValueToArray(marks, 0, 89);
		ArrayFunction.setValueToArray(marks, 4, 75);
		ArrayFunction.setValueToArray(marks, 3, 100);

		int mathsMarks = ArrayFunction.getvalueFromArray(marks, 3);
		System.out.println("Maths marks is:" + mathsMarks);

	}

}


